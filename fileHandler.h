#include <iostream>
#include <vector>
#include <fstream>

namespace FileHandler
{
	std::fstream file;
	std::vector<std::string> list;
	std::string path;

	void init(const char* filename)
	{
		file.open(filename, std::ios::in);
		std::string item;
		path = filename;

		while(std::getline(file, item))
			list.push_back(item);
		file.close();
	}
	
	void app(const char* string)
	{
		list.push_back(std::string(string));
	}

	void del(int index)
	{
		list.erase(list.begin() + index);
	}

	void write()
	{
		file.open(path, std::ios::out | std::ios::trunc);

		for (std::string s : list)
			file << s << '\n';
	}

	void print()
	{
		int i = 0;
		std::cout << '\n';
		for (std::string s : list)
			std::cout << i++ << "  " <<  s << '\n';
		std::cout << '\n';
	}

	void clear()
	{
		list.clear();	
	}

	std::vector<std::string> getList()
	{
		return list;
	}
}

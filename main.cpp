#include "fileHandler.h"

int main()
{
	FileHandler::init("~/.local/share/todo/.list");

	// main input loop
	std::string input, operation, argument;
	bool isRunning = true;
	while ( isRunning )
	{
		std::cout << "> ";
		std::getline(std::cin, input);


		if (input.find(' ') != std::string::npos) {
			argument = input.substr(input.find(' ') + 1);
			operation = input.substr(0, input.find(' '));
		} else {
			argument = "";	
			operation = input;
		}


		if(operation == "add")
			FileHandler::app(argument.c_str());
		else if (operation == "del")
			FileHandler::del(atoi(argument.c_str()));
		else if (operation == "list")
			FileHandler::print();
		else if (operation == "clear")
			FileHandler::clear();
		else if (operation == "exit")
		{
			FileHandler::write();
			isRunning = false;
		}
	}

	FileHandler::write();

	return 0;
}
